package buu.hattaya.mathgamebinding

import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import androidx.navigation.NavHost
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import buu.hattaya.mathgamebinding.databinding.FragmentPlayGroundBinding
import buu.hattaya.mathgamebinding.databinding.FragmentTitleBinding
import kotlin.random.Random

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [PlayGroundFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class PlayGroundFragment : Fragment() {
   private lateinit var binding: FragmentPlayGroundBinding
    val args: PlayGroundFragmentArgs by navArgs()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate<FragmentPlayGroundBinding>(inflater,
            R.layout.fragment_play_ground,container,false)

        timer()
        Game()
        setHasOptionsMenu(true)
        return binding.root
    }
    var correctScore: Int = 0
    var incorrectScore: Int = 0
    var numberOfClauses:Int = -1

    fun timer() {
        object : CountDownTimer(15000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                binding.txtTimer.setText("Time     " +millisUntilFinished / 1000)
            }
            override fun onFinish() {
                binding.txtTimer.setText("Time's finished !")
                var scoreRight = correctScore
                var scoreWrong = incorrectScore
                val action = PlayGroundFragmentDirections.actionPlayGroundFragmentToCategoryFragment(scoreRight, scoreWrong)
                findNavController().navigate(action)

            }
        }.start()
    }


    fun Game() {
        numberOfClauses ++

        val number1 = binding.txtNumber1
        val numberRandom1 =  Random.nextInt(0,9)
        number1.text = numberRandom1.toString()

        val number2 = binding.txtNumber2
        val numberRandom2 =  Random.nextInt(0,9)
        number2.text = numberRandom2.toString()

        val sign = args.sign.toString()
        var answer: Int = 0

        if (sign == "1") {
            val txtSign = binding.txtSign
            txtSign.text = "+"
            answer = numberRandom1 + numberRandom2
        }

        if (sign == "2") {
            val txtSign = binding.txtSign
            txtSign.text = "-"
            answer = numberRandom1 - numberRandom2
        }

        if (sign == "3") {
            val txtSign = binding.txtSign
            txtSign.text = "x"
            answer = numberRandom1 * numberRandom2
        }

        if (sign == "4") {
            val txtSign = binding.txtSign
            val allSign = Random.nextInt(0, 3)
            if (allSign == 1) {
                txtSign.text = "+"
                answer = numberRandom1 + numberRandom2
            } else if (allSign == 2) {
                txtSign.text = "-"
                answer = numberRandom1 - numberRandom2
            } else {
                txtSign.text = "x"
                answer = numberRandom1 * numberRandom2
            }

        }

        val amount = binding.txtAmount
        amount.text = numberOfClauses.toString()

        val position = Random.nextInt(0,3)

        val choice1 = binding.btnChoice1
        val choice2 = binding.btnChoice2
        val choice3 = binding.btnChoice3

        if (position == 1){
            choice1.text = (answer+1).toString()
            choice2.text = (answer-1).toString()
            choice3.text = answer.toString()
        } else if (position == 2) {
            choice1.text = (answer-1).toString()
            choice2.text = answer.toString()
            choice3.text = (answer+1).toString()
        } else {
            choice1.text = answer.toString()
            choice2.text = (answer-1).toString()
            choice3.text = (answer+1).toString()
        }

        val scoreCorrect = binding.txtpointRight
        scoreCorrect.text = correctScore.toString()
        val scoreIncorrect = binding.txtpointWrong
        scoreIncorrect.text = incorrectScore.toString()

        val declare = binding.txtDeclare

        fun addCorrect(score: Int) {
            correctScore ++
            scoreCorrect.text = correctScore.toString()
            declare.text = " Correct !!"
        }
        fun addIncorrect(score: Int) {
            incorrectScore ++
            scoreIncorrect.text = incorrectScore.toString()
            declare.text = " Incorrect !!"
        }

        choice1.setOnClickListener {
            if (choice1.text.toString() == answer.toString()) {
                addCorrect(correctScore)
                Game()
            } else {
                addIncorrect(incorrectScore)
                Game()
            }
        }
        choice2.setOnClickListener {
            if (choice2.text.toString() == answer.toString()) {
                addCorrect(correctScore)
                Game()
            } else {
                addIncorrect(incorrectScore)
                Game()
            }
        }
        choice3.setOnClickListener {
            if (choice3.text.toString() == answer.toString()) {
                addCorrect(correctScore)
                Game()
            } else {
                addIncorrect(incorrectScore)
                Game()
            }
        }


    }
}