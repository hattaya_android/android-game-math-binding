package buu.hattaya.mathgamebinding


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import buu.hattaya.mathgamebinding.databinding.FragmentCategoryBinding


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [CategoryFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class CategoryFragment : Fragment() {
    private lateinit var binding: FragmentCategoryBinding



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate<FragmentCategoryBinding>(inflater,
            R.layout.fragment_category,container,false)

        var sign: Int = 0
        var onClick: Int = 0


        var Correct = CategoryFragmentArgs.fromBundle(requireArguments()).scoreRight.toString()
        var Incorrect = CategoryFragmentArgs.fromBundle(requireArguments()).scoreWrong.toString()

        binding.txtCorrect.text = Correct
        binding.txtIncorrect.text = Incorrect
        binding.btnPlus.setOnClickListener{
            sign = 1
            onClick = 1
        }

        binding.btnSubtract.setOnClickListener {
            sign = 2
            onClick = 1
        }

        binding.btnMultiply.setOnClickListener{
            sign = 3
            onClick = 1
        }

        binding.btnAll.setOnClickListener{
            sign = 4
            onClick = 1
        }

        binding.btnStart.setOnClickListener{
            if(onClick == 0) {
                //Toast.makeText(CategoryFragment@this, "Please select a category.", Toast.LENGTH_LONG).show()
            } else {
                val action = CategoryFragmentDirections.actionCategoryFragmentToPlayGroundFragment(sign)
                it.findNavController().navigate(action)
            }
        }

        setHasOptionsMenu(true)
        return binding.root
    }

}